import React, { Component } from 'react'
import { connect } from 'react-redux'
import { GetAllStatus } from 'src/api/actions/status';
import { CreateToko, GetAllToko } from 'src/api/actions/toko'
import TokoCreateNew from './TokoCreateNew';
import TokoTable from './TokoTable'
import Swal from 'sweetalert2'
import TokoDetail from './TokoDetail';
import { ToastContainer } from 'react-toastify';

const mapStateToProps = (state) => {
  return {
    GetAllToko: state.Toko.GetAllToko,
    errorGetAllToko: state.Toko.errorGetAllToko,
    CreateToko: state.Toko.CreateToko,
    errorCreateToko: state.Toko.errorCreateToko,
    UpdateToko: state.Toko.UpdateToko,
    errorUpdateToko: state.Toko.errorUpdateToko,
    DeleteToko: state.Toko.DeleteToko,
    errorDeleteToko: state.Toko.errorDeleteToko,
    tokoRoute: state.ReduxState.tokoRoute,
    lat: state.ReduxState.lat,
    lng: state.ReduxState.lng,
  };
};
class Toko extends Component {

  componentDidMount() {
    this.props.dispatch(GetAllToko({ limit: 1000 }))
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.GetAllToko && this.props.GetAllToko.rows.length)
      if (prevProps.GetAllToko !== this.props.GetAllToko) {
        this.props.dispatch(GetAllStatus())
      }
    if (this.props.CreateToko || this.props.UpdateToko || this.props.DeleteToko)
      if (prevProps.CreateToko !== this.props.CreateToko ||
        prevProps.UpdateToko !== this.props.UpdateToko ||
        prevProps.DeleteToko !== this.props.DeleteToko) {
        Swal.fire({
          text: (this.props.CreateToko && this.props.CreateToko.message) || (this.props.UpdateToko && this.props.UpdateToko.message) || (this.props.DeleteToko && this.props.DeleteToko.message),
          icon: "success",
        });
        this.props.dispatch({ type: "set", lat: false, lng: false })
        this.props.dispatch({ type: "CREATE_TOKO", payload: { data: false, errorMessage: false, }, })
        this.props.dispatch({ type: "UPDATE_TOKO", payload: { data: false, errorMessage: false, }, })
        this.props.dispatch({ type: "DELETE_TOKO", payload: { data: false, errorMessage: false, }, })
        this.props.dispatch(GetAllToko({ limit: 1000 }))
      }

  }
  handleSubmit(data) {
    const dataReq = {
      name: data.name,
      status: Number(data.status),
      address: [{
        name: data.nameLengkap,
        phone: data.phone,
        address: data.address,
        provinsi_id: data.provinsi_id,
        kota_kabupaten_id: data.kota_kabupaten_id,
        kecamatan_id: data.kecamatan_id,
        kelurahan_id: data.kelurahan_id,
        zip_code: data.zip_code,
        latitude: String(this.props.lat),
        longitude: String(this.props.lng),
        type: Number(data.type),
        status: Number(data.status_alamat),
      }]
    }
    this.props.dispatch(CreateToko(dataReq))
    this.props.dispatch({ type: "set", tokoRoute: "table" })
    data = ""

  }
  render() {
    const { tokoRoute } = this.props
    return (
      <>
        <ToastContainer position="top-right" />
        {tokoRoute === "table" && <TokoTable />}
        {tokoRoute === "create" && <TokoCreateNew onSubmit={(data) => this.handleSubmit(data)} />}
        {tokoRoute === "detail" && <TokoDetail />}

      </>
    )
  }
}

export default connect(mapStateToProps)(Toko)