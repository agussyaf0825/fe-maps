import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { Button } from '@windmill/react-ui'
import { GetAllKecamatan } from 'src/api/actions/daerah/kecamatan';
import { GetAllKelurahan } from 'src/api/actions/daerah/kelurahan';
import { GetAllKotaKabupaten } from 'src/api/actions/daerah/kotaKabupaten';
import { GetAllProvinsi } from 'src/api/actions/daerah/provinsi';
import { GetAllStatus } from 'src/api/actions/status';
import { GetAllType } from 'src/api/actions/type';
import { FieldFormInput, FieldFormInputSelect, FieldFormInputSpan, } from 'src/ReduxFrom/FIeldFromInput';
import Maps from '../maps/Maps';
import TokoValidation from 'src/ReduxFormValidation/TokoValidation';
const { REACT_APP_MAPS_APIKEYS } = process.env

const mapStateToProps = (state) => {
    return {
        getAllStatus: state.Status.getAllStatus,
        errorGetAllStatus: state.Status.errorGetAllStatus,
        getAllType: state.Type.getAllType,
        errorGetAllType: state.Type.errorGetAllType,
        GetAllProvinsi: state.Daerah.GetAllProvinsi,
        errorGetAllProvinsi: state.Daerah.errorGetAllProvinsi,
        GetAllKotaKabupaten: state.Daerah.GetAllKotaKabupaten,
        errorGetAllKotaKabupaten: state.Daerah.errorGetAllKotaKabupaten,
        GetAllKecamatan: state.Daerah.GetAllKecamatan,
        errorGetAllKecamatan: state.Daerah.errorGetAllKecamatan,
        GetAllKelurahan: state.Daerah.GetAllKelurahan,
        errorGetAllKelurahan: state.Daerah.errorGetAllKelurahan,
        showModal: state.ReduxState.showModal,
        lat: state.ReduxState.lat,
        lng: state.ReduxState.lng,
        tokoRoute: state.ReduxState.tokoRoute,
    };
};
class TokoCreateNew extends Component {
    constructor(props) {
        super(props);
        this.state = {
            maps: false,
            kota: true,
            kecamatan: true,
            kelurahan: true,
            valueName: "",
            valueAlamat: "",
            valueProvinsi: "",
            valueKotaKabupaten: "",
            valueKecamatan: "",
            valueKelurahan: "",
        };
    }
    componentDidMount() {
        console.log(REACT_APP_MAPS_APIKEYS, "REACT_APP_MAPS_APIKEYS")

        this.props.dispatch(GetAllStatus())
        this.props.dispatch(GetAllType())
        this.props.dispatch(GetAllProvinsi())
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.GetAllKotaKabupaten) {
            if (prevProps.GetAllKotaKabupaten !== this.props.GetAllKotaKabupaten) {
                this.setState({ kota: false })
            }
        }
        if (this.props.GetAllKecamatan) {
            if (prevProps.GetAllKecamatan !== this.props.GetAllKecamatan) {
                this.setState({ kecamatan: false })
            }
        }
        if (this.props.GetAllKelurahan) {
            if (prevProps.GetAllKelurahan !== this.props.GetAllKelurahan) {
                this.setState({ kelurahan: false })
            }
        }
        if (this.props.showModal === false) {
            if (prevProps.showModal !== this.props.showModal) {
                this.setState({ maps: false })
            }
        }

    }
    name(e) {
        this.setState({ valueName: e.target.value })
    }
    alamat(e) {
        this.setState({ valueAlamat: e.target.value })
    }
    provinsi(e) {
        if (e.target.value === "") {
            this.setState({ kota: true, kecamatan: true, kelurahan: true, valueProvinsi: "" })
        } else {
            this.setState({ valueProvinsi: this.props.GetAllProvinsi.rows.filter(i => i.id === e.target.value).map(i => i.name).toString() })
            this.props.dispatch(GetAllKotaKabupaten({ provinsi_id: e.target.value }))
        }
    }
    kotaKabupaten(e) {
        console.log(e.target.value, "kotaKabupaten")
        if (e.target.value === "") {
            this.setState({ kecamatan: true, kelurahan: true, valueKotaKabupaten: "" })
        } else {
            this.setState({ valueKotaKabupaten: this.props.GetAllKotaKabupaten.rows.filter(i => i.id === e.target.value).map(i => i.name).toString() })
            this.props.dispatch(GetAllKecamatan({ kota_kabupaten_id: e.target.value }))
        }

    }
    kecamatan(e) {
        console.log(e.target.value, "kecamatan")
        if (e.target.value === "") {
            this.setState({ kelurahan: true, valueKecamatan: "" })
        } else {
            this.setState({ valueKecamatan: this.props.GetAllKecamatan.rows.filter(i => i.id === e.target.value).map(i => i.name).toString() })
            this.props.dispatch(GetAllKelurahan({ kecamatan_id: e.target.value }))
        }
    }
    kelurahan(e) {
        if (e.target.value === "") {
            this.setState({ valueKelurahan: "" })
        } else {
            this.setState({ valueKelurahan: this.props.GetAllKelurahan.rows.filter(i => i.id === e.target.value).map(i => i.name).toString() })
        }
    }

    maps() {
        this.props.dispatch({ type: "set", showModal: true })
        this.setState({ maps: true })
    }

    render() {
        const { tokoRoute, handleSubmit, lat, lng, getAllStatus, getAllType, GetAllProvinsi, GetAllKotaKabupaten, GetAllKecamatan, GetAllKelurahan } = this.props
        const { maps, kota, kecamatan, kelurahan, valueName, valueAlamat, valueProvinsi, valueKotaKabupaten, valueKecamatan, valueKelurahan } = this.state
        console.log({ lat, lng }, " lat, lng")
        return (
            tokoRoute === "create" &&
            <>
                {maps &&
                    <Maps
                        valueName={valueName}
                        valueAlamat={valueAlamat}
                        valueProvinsi={valueProvinsi}
                        valueKotaKabupaten={valueKotaKabupaten}
                        valueKecamatan={valueKecamatan}
                        valueKelurahan={valueKelurahan}
                    />
                }
                <div className='mt-2'>
                    <div className=" flex justify-center mt-10">
                        <div className=" p-4 borderBox rounded-3xl border-2 border-gray-900 dark:border-white  " style={{ width: "754px", }}>
                            <u><div className='poppins-800 text-center pb-2 bold'>Add New Toko</div></u>
                            <form onSubmit={handleSubmit}>
                                <div className="flex justify-between py-2">
                                    <div className="relative w-full mr-2">
                                        <Field
                                            label="Nama Toko"
                                            name="name"
                                            placeholder="Nama"
                                            autoComplete="off"
                                            component={FieldFormInput}
                                            validasi={true}
                                            type="text"
                                            onChange={this.name.bind(this)}
                                        />
                                    </div>
                                    <div className="relative w-full ml-2">
                                        <Field
                                            label="Status Toko"
                                            name="status"
                                            component={FieldFormInputSelect}
                                            validasi={true}
                                            showModal={true}
                                        >
                                            <option className="capitalize dark:text-cool-gray-200" value="">Select Status Toko</option>
                                            {getAllStatus && getAllStatus.map((data, index) => (
                                                <option className="dark:text-cool-gray-200 capitalize" value={data.id} key={index} >{data.name}</option>
                                            ))
                                            }
                                        </Field>
                                    </div>
                                </div>
                                        <div className="relative w-full  py-2">
                                            <Field
                                                label="Alamat Lengkap"
                                                name="address"
                                                placeholder="Alamat Lengkap"
                                                autoComplete="off"
                                                component={FieldFormInputSpan}
                                                onChange={this.alamat.bind(this)}
                                                validasi={true}
                                                type="text"
                                            />
                                        </div>

                                        <div className="flex justify-between py-2">
                                            <div className="relative w-full mr-2">
                                                <Field
                                                    label="Nama Lengkap "
                                                    name="nameLengkap"
                                                    placeholder="Nama Lengkap  "
                                                    autoComplete="off"
                                                    component={FieldFormInput}
                                                    validasi={true}
                                                    type="text"
                                                />
                                            </div>
                                            <div className="relative w-full mr-2">
                                                <Field
                                                    label="Nomor Telepon"
                                                    name="phone"
                                                    placeholder="Nomor Telepon "
                                                    autoComplete="off"
                                                    component={FieldFormInput}
                                                    validasi={true}
                                                    type="text"
                                                />
                                            </div>
                                        </div>

                                        <div className="flex justify-between py-2">
                                            <div className="relative w-full mr-2">
                                                <Field
                                                    label="Provinsi"
                                                    name="provinsi_id"
                                                    component={FieldFormInputSelect}
                                                    validasi={true}
                                                    showModal={true}
                                                    onChange={this.provinsi.bind(this)}
                                                >
                                                    <option className="capitalize dark:text-cool-gray-200" value="">Select Provinsi</option>
                                                    {GetAllProvinsi && GetAllProvinsi.rows.map((data, index) => (
                                                        <option className="dark:text-cool-gray-200 capitalize" value={data.id} key={index} >{data.name}</option>
                                                    ))
                                                    }
                                                </Field>
                                            </div>
                                            <div className="relative w-full mr-2">
                                                <Field
                                                    label="Kota Kabupaten"
                                                    name="kota_kabupaten_id"
                                                    component={FieldFormInputSelect}
                                                    disabled={kota}
                                                    validasi={true}
                                                    showModal={true}
                                                    onChange={this.kotaKabupaten.bind(this)}
                                                >
                                                    <option className="capitalize dark:text-cool-gray-200" value="">Select Kota Kabupaten</option>
                                                    {!kota && GetAllKotaKabupaten && GetAllKotaKabupaten.rows.map((data, index) => (
                                                        <option className="dark:text-cool-gray-200 capitalize" value={data.id} key={index} >{data.name}</option>
                                                    ))
                                                    }
                                                </Field>
                                            </div>
                                        </div>

                                        <div className="flex justify-between py-2">
                                            <div className="relative w-full mr-2">
                                                <Field
                                                    label="Kecamatan"
                                                    name="kecamatan_id"
                                                    component={FieldFormInputSelect}
                                                    disabled={kecamatan}
                                                    validasi={true}
                                                    showModal={true}
                                                    onChange={this.kecamatan.bind(this)}
                                                >
                                                    <option className="capitalize dark:text-cool-gray-200" value="">Select Kecamatan</option>
                                                    {!kecamatan && GetAllKecamatan && GetAllKecamatan.rows.map((data, index) => (
                                                        <option className="dark:text-cool-gray-200 capitalize" value={data.id} key={index} >{data.name}</option>
                                                    ))
                                                    }
                                                </Field>
                                            </div>
                                            <div className="relative w-full mr-2">
                                                <Field
                                                    label="Kelurahan"
                                                    name="kelurahan_id"
                                                    component={FieldFormInputSelect}
                                                    disabled={kelurahan}
                                                    validasi={true}
                                                    showModal={true}
                                                    onChange={this.kelurahan.bind(this)}
                                                >
                                                    <option className="capitalize dark:text-cool-gray-200" value="">Select Kelurahan</option>
                                                    {!kelurahan && GetAllKelurahan && GetAllKelurahan.rows.map((data, index) => (
                                                        <option className="dark:text-cool-gray-200 capitalize" value={data.id} key={index} >{data.name}</option>
                                                    ))
                                                    }
                                                </Field>
                                            </div>
                                        </div>
                                   
                                <div className="flex justify-between py-2">
                                    <div className="relative w-full mr-2">
                                        <Field
                                            label="Kode Pos"
                                            name="zip_code"
                                            placeholder="Kode Pos"
                                            autoComplete="off"
                                            component={FieldFormInput}
                                            type="text"
                                        />
                                    </div>
                                    <div className="relative w-full mr-2">
                                        <Field
                                            label="Type Alamat"
                                            name="type"
                                            component={FieldFormInputSelect}
                                            validasi={true}
                                            showModal={true}
                                        >
                                            <option className="capitalize dark:text-cool-gray-200" value="">Select Type Alamat</option>
                                            {getAllType && getAllType.map((data, index) => (
                                                <option className="dark:text-cool-gray-200 capitalize" value={data.id} key={index} >{data.name}</option>
                                            ))
                                            }
                                        </Field>
                                    </div>
                                    
                                    <div className="relative w-full mr-2">
                                        <Field
                                            label="Status Alamat"
                                            name="status_alamat"
                                            component={FieldFormInputSelect}
                                            validasi={true}
                                            showModal={true}
                                        >
                                            <option className="capitalize dark:text-cool-gray-200" value="">Select Status Alamat</option>
                                            {getAllStatus && getAllStatus.map((data, index) => (
                                                <option className="dark:text-cool-gray-200 capitalize" value={data.id} key={index} >{data.name}</option>
                                            ))
                                            }
                                        </Field>
                                    </div>
                                </div>
                                <div className='flex justify-end py-2 px-4'>
                                    <button disabled={valueAlamat !== "" ? false : true}
                                        type="button"
                                        className={(valueAlamat !== "" ? "cursor-pointer" : "cursor-auto") + ' bg-custom-blue1 borderBox text-white rounded-lg shadow p-2 mr-2'}
                                        onClick={this.maps.bind(this)}
                                    > Maps</button>
                                    <Button className="w-full sm:w-auto mr-2" layout="outline" onClick={() => this.props.dispatch({ type: "set", tokoRoute: "table" })}>
                                        Cancel
                                    </Button>
                                    {lat === "" && lng === "" ?
                                        <Button disabled={valueAlamat !== "" ? false : true} className="ml-2 w-full sm:w-auto" onClick={this.maps.bind(this)}>SAVE</Button>
                                        :
                                        <Button type='submit' className="ml-2 w-full sm:w-auto" >SAVE</Button>
                                    }
                                </div>
                            </form>

                        </div>
                    </div>
                </div >
            </>
        )
    }
}

TokoCreateNew = reduxForm({
    form: "formTokoCreateNew",
    validate: TokoValidation,
    enableReinitialize: true,
})(TokoCreateNew);
export default connect(mapStateToProps, null)(TokoCreateNew)