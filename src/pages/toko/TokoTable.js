import React, { Component } from 'react'
import {
  Table,
  TableHeader,
  TableBody,
  TableRow,
  TableCell,
  Pagination
} from '@windmill/react-ui'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { ClipLoader } from 'react-spinners';
import moment from 'moment'
import Swal from 'sweetalert2'
import { DeleteToko, GetIdToko, UpdateToko } from 'src/api/actions/toko';
import Maps from '../maps/Maps';
import TokoEdit from './TokoEdit';
import { GetAllAddress } from 'src/api/actions/address';
const mapStateToProps = (state) => {
  return {
    getAllStatus: state.Status.getAllStatus,
    errorGetAllStatus: state.Status.errorGetAllStatus,
    GetAllToko: state.Toko.GetAllToko,
    errorGetAllToko: state.Toko.errorGetAllToko,
    GetIdToko: state.Toko.GetIdToko,
    errorGetIdToko: state.Toko.errorGetIdToko,
  };
};

class TokoTable extends Component {
  onPageChange(p) {

  }
  viewMaps() {
    this.props.dispatch(GetAllAddress({ limit: 1000 }))
    this.props.dispatch({ type: "set", showModal: true, mapsRoute: "getAll" })
  }
  HandleClickMaps(tokoUuid) {
    this.props.dispatch(GetIdToko(tokoUuid))
    this.props.dispatch({ type: "set", showModal: true, mapsRoute: "getOne" })
  }
  HandleClickDetail(tokoUuid) {
    this.props.history.replace(`/toko/address/${tokoUuid}`)

  }
  HandleClickEdit(tokoUuid) {
    this.props.dispatch(GetIdToko(tokoUuid))
    this.props.dispatch({ type: "set", tokoEdit: true })

  }
  HandleClickDelete(tokoUuid) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.props.dispatch(DeleteToko(tokoUuid))
      }
    })
  }
  handleSubmit(data) {
    this.props.dispatch(UpdateToko(this.props.GetIdToko.tokoUuid, data))
    this.props.dispatch({ type: "set", tokoEdit: false })
    this.props.dispatch({ type: "GET_ID_TOKO", payload: { data: false, errorMessage: false, } })
  }
  render() {
    const { GetAllToko, errorGetAllToko, getAllStatus } = this.props
    return (
      <>
        <TokoEdit onSubmit={(data) => this.handleSubmit(data)} />
        <Maps />
        <div className='mt-4'>
          <div className='mb-4'>
            <button className="poppins-500 bg-sidebar py-1 px-2 rounded-lg border borderBox dark:bg-gray-400 dark:border-white  hover:border-white hover:shadow-none hover:bg-cool-gray-300 dark:hover:bg-cool-gray-400"
              onClick={() => this.props.dispatch({ type: "set", tokoRoute: "create" })}
            >
              Add New Toko
            </button>
            <button className="ml-4 poppins-500 bg-sidebar py-1 px-2 rounded-lg border borderBox dark:bg-gray-400 dark:border-white  hover:border-white hover:shadow-none hover:bg-cool-gray-300 dark:hover:bg-cool-gray-400"
              onClick={() => this.viewMaps()}
            >
              View Maps
            </button>
          </div>
        </div>
        <Table>
          <TableHeader>
            <TableRow className="bg-white dark:bg-cool-gray-800 border-b  dark:border-white text-center">
              <TableCell className="py-4 poppins-500 text-black text-sm dark:text-white poppins-500">TOKO UUID</TableCell>
              <TableCell className="py-4 poppins-500 text-black text-sm dark:text-white poppins-500">Date</TableCell>
              <TableCell className="py-4 poppins-500 text-black text-sm dark:text-white poppins-500">NAME</TableCell>
              <TableCell className="py-4 poppins-500 text-black text-sm dark:text-white poppins-500">STATUS</TableCell>
              <TableCell className="py-4 poppins-500 text-black text-sm dark:text-white poppins-500">Total Alamat</TableCell>
              <TableCell className="py-4 poppins-500 text-black text-sm dark:text-white poppins-500">Action</TableCell>
            </TableRow>
          </TableHeader>
          {GetAllToko ?
            GetAllToko.rows.length ? (
              GetAllToko.rows.map((data, id) => (
                <TableBody key={id}>
                  <TableRow className="bg-white dark:bg-cool-gray-800  dark:border-white text-center cursor-pointer" >
                    <TableCell className=" text-gray-900 dark:text-white poppins-300 border-b  dark:border-white">
                      {data.tokoUuid}
                    </TableCell>
                    <TableCell className=" text-gray-900 dark:text-white poppins-300 border-b  dark:border-white">
                      {moment(data.createdAt).format('YYYY-MM-DD HH:mm:ss')}
                    </TableCell>
                    <TableCell className=" text-gray-900 dark:text-white poppins-300 border-b  dark:border-white">
                      {data.name}
                    </TableCell>
                    <TableCell className=" text-gray-900 dark:text-white poppins-300 border-b  dark:border-white">
                      {data.status && getAllStatus && getAllStatus.filter(i => i.id === data.status).map(i => i.name)}
                    </TableCell>
                    <TableCell className=" text-gray-900 dark:text-white poppins-300 border-b  dark:border-white">
                      {data.addresses.length}
                    </TableCell>
                    <TableCell className=" text-gray-900 dark:text-white poppins-300 border-b  dark:border-white">
                      {data.addresses.length !== 0 &&
                        <button
                          className="has-tooltip dark:text-white cursor-pointer font-bold px-2 rounded-lg text-xl outline-none focus:outline-none mx-1 "
                          onClick={() => this.HandleClickMaps(data.tokoUuid)}
                        >
                          <i className="fas fa-map-marked-alt"></i>
                        </button>
                      }
                      <button
                        className="has-tooltip dark:text-white cursor-pointer font-bold px-2 rounded-lg text-xl outline-none focus:outline-none  mx-1 "
                        onClick={() => this.HandleClickDetail(data.tokoUuid)}
                      >
                        <i className="fas fa-eye"></i>
                      </button>
                      <button
                        className="has-tooltip dark:text-white cursor-pointer font-bold px-2 rounded-lg text-xl outline-none focus:outline-none  mx-1  "
                        onClick={() => this.HandleClickEdit(data.tokoUuid)}
                      >
                        <i className="fas fa-edit mr-1" />
                      </button>
                      <button
                        className="has-tooltip dark:text-white cursor-pointer font-bold px-2 rounded-lg text-xl outline-none focus:outline-none  mx-1 "
                        onClick={() => this.HandleClickDelete(data.tokoUuid)}
                      >
                        <i className="fas fa-trash-alt mr-1" />
                      </button>
                    </TableCell>
                  </TableRow>
                </TableBody>
              ))
            ) : (
              <TableBody>
                <TableRow className="bg-white dark:bg-cool-gray-800 text-center">
                  <TableCell colSpan="6" className=" text-gray-900 dark:text-white poppins-300 border-b   dark:border-white">
                    {errorGetAllToko ?
                      <h1 className="text-red-500">{errorGetAllToko}</h1>
                      :
                      <h1>Data Not Found</h1>
                    }
                  </TableCell>
                </TableRow>
              </TableBody>
            )
            :
            <TableBody>
              <TableRow className="bg-white dark:bg-cool-gray-800 text-center">
                <TableCell colSpan="6" className=" text-gray-900 dark:text-white poppins-300 border-b   dark:border-white">
                  Loading ... <ClipLoader size={15} color={"#000000"} />
                </TableCell>
              </TableRow>
            </TableBody>
          }
        </Table>
        <div className="pt-4">
          <Pagination
            totalResults={GetAllToko.count}
            resultsPerPage={10}
            onChange={this.onPageChange.bind(this)}
            label="Table Pagination" />
        </div>
      </>
    )
  }
}

export default connect(mapStateToProps)(withRouter(TokoTable))