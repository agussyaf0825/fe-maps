import React, { Component } from 'react'
import { Modal, ModalBody, ModalFooter, Button } from '@windmill/react-ui'
import { connect } from 'react-redux'
import TokoValidation from 'src/ReduxFormValidation/TokoValidation';
import { Field, reduxForm } from 'redux-form';
import { FieldFormInput, FieldFormInputSelect } from 'src/ReduxFrom/FIeldFromInput';

const mapStateToProps = (state) => {
  return {
    GetIdToko: state.Toko.GetIdToko,
    errorGetIdToko: state.Toko.errorGetIdToko,
    tokoEdit: state.ReduxState.tokoEdit,
    getAllStatus: state.Status.getAllStatus,
    errorGetAllStatus: state.Status.errorGetAllStatus,
    initialValues: {
      name: state.Toko.GetIdToko && state.Toko.GetIdToko.name,
      status: state.Toko.GetIdToko && state.Toko.GetIdToko.status,
    }
  };
};
class TokoEdit extends Component {
  Cancel() {
    this.props.dispatch({ type: "set", tokoEdit: false })
  }
  render() {
    const { tokoEdit, handleSubmit, getAllStatus } = this.props
    return (
      <>
        <Modal isOpen={tokoEdit} onClose={() => this.Cancel()} >
          <form onSubmit={handleSubmit}>
            <ModalBody>
              <div className="flex justify-between py-2">
                <div className="relative w-full mr-2">
                  <Field
                    label="Nama Toko"
                    name="name"
                    placeholder="Nama"
                    autoComplete="off"
                    component={FieldFormInput}
                    validasi={true}
                    type="text"
                  />
                </div>
                <div className="relative w-full ml-2">
                  <Field
                    label="Status Toko"
                    name="status"
                    component={FieldFormInputSelect}
                    validasi={true}
                    showModal={true}
                  >
                    <option className="capitalize dark:text-cool-gray-200" value="">Select Status Toko</option>
                    {getAllStatus && getAllStatus.map((data, index) => (
                      <option className="dark:text-cool-gray-200 capitalize" value={data.id} key={index} >{data.name}</option>
                    ))
                    }
                  </Field>
                </div>
              </div>
            </ModalBody>
            <ModalFooter>
              <Button className="w-full sm:w-auto" layout="outline" onClick={() => this.Cancel()}>
                Cancel
              </Button>
              <Button type="submit" className="w-full sm:w-auto" >
                Update
              </Button>
            </ModalFooter>
          </form>
        </Modal>
      </>
    )
  }
}
TokoEdit = reduxForm({
  form: "formTokoEdit",
  validate: TokoValidation,
  enableReinitialize: true,
})(TokoEdit);
export default connect(mapStateToProps)(TokoEdit)