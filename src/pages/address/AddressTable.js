import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
    Table,
    TableHeader,
    TableBody,
    TableRow,
    TableCell,
    Pagination
} from '@windmill/react-ui'
import moment from 'moment'
import { GetAllProvinsi } from 'src/api/actions/daerah/provinsi';
import { GetAllKotaKabupaten } from 'src/api/actions/daerah/kotaKabupaten';
import { GetAllKecamatan } from 'src/api/actions/daerah/kecamatan';
import { GetAllKelurahan } from 'src/api/actions/daerah/kelurahan';
import { ClipLoader } from 'react-spinners';
import { GetAllStatus } from 'src/api/actions/status';
import { GetAllType } from 'src/api/actions/type';
import { DeleteAddress, GetIdAddress } from 'src/api/actions/address';
import Swal from 'sweetalert2'

const mapStateToProps = (state) => {
    return {
        GetAllAddress: state.Address.GetAllAddress,
        errorGetAllAddress: state.Address.errorGetAllAddress,
        GetAllToko: state.Toko.GetAllToko,
        errorGetAllToko: state.Toko.errorGetAllToko,
        GetAllProvinsi: state.Daerah.GetAllProvinsi,
        errorGetAllProvinsi: state.Daerah.errorGetAllProvinsi,
        GetAllKotaKabupaten: state.Daerah.GetAllKotaKabupaten,
        errorGetAllKotaKabupaten: state.Daerah.errorGetAllKotaKabupaten,
        GetAllKecamatan: state.Daerah.GetAllKecamatan,
        errorGetAllKecamatan: state.Daerah.errorGetAllKecamatan,
        GetAllKelurahan: state.Daerah.GetAllKelurahan,
        errorGetAllKelurahan: state.Daerah.errorGetAllKelurahan,
        getAllStatus: state.Status.getAllStatus,
        errorGetAllStatus: state.Status.errorGetAllStatus,
        getAllType: state.Type.getAllType,
        errorGetAllType: state.Type.errorGetAllType,
    };
};

class AddressTable extends Component {
    onPageChange(p) {

    }
    componentDidMount() {
        this.props.dispatch(GetAllStatus())
        this.props.dispatch(GetAllType())
    }
    componentDidUpdate(prevProps, prevState) {
        if (this.props.GetAllAddress) {
            if (prevProps.GetAllAddress !== this.props.GetAllAddress) {
                const provinsi_id = this.props.GetAllAddress.rows.map(i => i.provinsi_id)
                const kota_kabupaten_id = this.props.GetAllAddress.rows.map(i => i.kota_kabupaten_id)
                const kecamatan_id = this.props.GetAllAddress.rows.map(i => i.kecamatan_id)
                this.props.dispatch(GetAllProvinsi({ limit: 1000 }))
                this.props.dispatch(GetAllKotaKabupaten({ provinsi_id: JSON.stringify(provinsi_id), limit: 1000 }))
                this.props.dispatch(GetAllKecamatan({ kota_kabupaten_id: JSON.stringify(kota_kabupaten_id), limit: 1000 }))
                this.props.dispatch(GetAllKelurahan({ kecamatan_id: JSON.stringify(kecamatan_id), limit: 1000 }))
            }

        }
    }
    HandleClickMaps(addressUuid) {
        this.props.dispatch(GetIdAddress(addressUuid))
        this.props.dispatch({ type: "set", showModal: true, mapsRoute: "getOne" })
    }

    HandleClickEdit(addressUuid) {
        this.props.dispatch(GetIdAddress(addressUuid))
        this.props.dispatch({ type: "set", addressRoute: "edit" })

    }
    HandleClickDelete(addressUuid) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                this.props.dispatch(DeleteAddress(addressUuid))
            }
        })
    }
    render() {
        const { GetAllAddress, errorGetAllAddress, getAllStatus, getAllType, GetAllProvinsi, GetAllKotaKabupaten, GetAllKecamatan, GetAllKelurahan } = this.props

        return (
            <>
                <Table>
                    <TableHeader>
                        <TableRow className="bg-white dark:bg-cool-gray-800 border-b  dark:border-white text-center">
                            <TableCell className="py-4 poppins-500 text-black text-sm dark:text-white poppins-500">Address UUID</TableCell>
                            <TableCell className="py-4 poppins-500 text-black text-sm dark:text-white poppins-500">Date</TableCell>
                            <TableCell className="py-4 poppins-500 text-black text-sm dark:text-white poppins-500">NAME</TableCell>
                            <TableCell className="py-4 poppins-500 text-black text-sm dark:text-white poppins-500">Phone</TableCell>
                            <TableCell className="py-4 poppins-500 text-black text-sm dark:text-white poppins-500">Alamat</TableCell>
                            <TableCell className="py-4 poppins-500 text-black text-sm dark:text-white poppins-500">Kelurahan</TableCell>
                            <TableCell className="py-4 poppins-500 text-black text-sm dark:text-white poppins-500">Kecamatan</TableCell>
                            <TableCell className="py-4 poppins-500 text-black text-sm dark:text-white poppins-500">kota Kabupaten</TableCell>
                            <TableCell className="py-4 poppins-500 text-black text-sm dark:text-white poppins-500">provinsi</TableCell>
                            <TableCell className="py-4 poppins-500 text-black text-sm dark:text-white poppins-500">kode Pos</TableCell>
                            <TableCell className="py-4 poppins-500 text-black text-sm dark:text-white poppins-500">Type</TableCell>
                            <TableCell className="py-4 poppins-500 text-black text-sm dark:text-white poppins-500">STATUS</TableCell>
                            <TableCell className="py-4 poppins-500 text-black text-sm dark:text-white poppins-500">Action</TableCell>
                        </TableRow>
                    </TableHeader>
                    {GetAllAddress ?
                        GetAllAddress.rows.length ? (
                            GetAllAddress.rows.map((data, id) => (
                                <TableBody key={id}>
                                    <TableRow className="bg-white dark:bg-cool-gray-800  dark:border-white text-center cursor-pointer" >
                                        <TableCell className=" text-gray-900 dark:text-white poppins-300 border-b  dark:border-white">
                                            {data.addressUuid}
                                        </TableCell>
                                        <TableCell className=" text-gray-900 dark:text-white poppins-300 border-b  dark:border-white">
                                            {moment(data.createdAt).format('YYYY-MM-DD HH:mm:ss')}
                                        </TableCell>
                                        <TableCell className=" text-gray-900 dark:text-white poppins-300 border-b  dark:border-white">
                                            {data.name}
                                        </TableCell>
                                        <TableCell className=" text-gray-900 dark:text-white poppins-300 border-b  dark:border-white">
                                            {data.phone}
                                        </TableCell>
                                        <TableCell className=" text-gray-900 dark:text-white poppins-300 border-b  dark:border-white">
                                            {data.address}
                                        </TableCell>
                                        <TableCell className=" text-gray-900 dark:text-white poppins-300 border-b  dark:border-white">
                                            {data.status && GetAllProvinsi && GetAllProvinsi.rows.filter(i => i.id === data.provinsi_id).map(i => i.name)}
                                        </TableCell>
                                        <TableCell className=" text-gray-900 dark:text-white poppins-300 border-b  dark:border-white">
                                            {data.status && GetAllKotaKabupaten && GetAllKotaKabupaten.rows.filter(i => i.id === data.kota_kabupaten_id).map(i => i.name)}
                                        </TableCell>
                                        <TableCell className=" text-gray-900 dark:text-white poppins-300 border-b  dark:border-white">
                                            {data.status && GetAllKecamatan && GetAllKecamatan.rows.filter(i => i.id === data.kecamatan_id).map(i => i.name)}
                                        </TableCell>
                                        <TableCell className=" text-gray-900 dark:text-white poppins-300 border-b  dark:border-white">
                                            {data.status && GetAllKelurahan && GetAllKelurahan.rows.filter(i => i.id === data.kelurahan_id).map(i => i.name)}
                                        </TableCell>
                                        <TableCell className=" text-gray-900 dark:text-white poppins-300 border-b  dark:border-white">
                                            {data.zip_code}
                                        </TableCell>
                                        <TableCell className=" text-gray-900 dark:text-white poppins-300 border-b  dark:border-white">
                                            {data.status && getAllType && getAllType.filter(i => i.id === data.type).map(i => i.name)}
                                        </TableCell>
                                        <TableCell className=" text-gray-900 dark:text-white poppins-300 border-b  dark:border-white">
                                            {data.status && getAllStatus && getAllStatus.filter(i => i.id === data.status).map(i => i.name)}
                                        </TableCell>
                                        <TableCell className=" text-gray-900 dark:text-white poppins-300 border-b  dark:border-white">
                                            <button
                                                className="has-tooltip dark:text-white cursor-pointer font-bold px-2 rounded-lg text-xl outline-none focus:outline-none mx-1 "
                                                onClick={() => this.HandleClickMaps(data.addressUuid)}
                                            >
                                                <i className="fas fa-map-marked-alt"></i>
                                            </button>
                                            <button
                                                className="has-tooltip dark:text-white cursor-pointer font-bold px-2 rounded-lg text-xl outline-none focus:outline-none  mx-1  "
                                                onClick={() => this.HandleClickEdit(data.addressUuid)}
                                            >
                                                <i className="fas fa-edit mr-1" />
                                            </button>
                                            <button
                                                className="has-tooltip dark:text-white cursor-pointer font-bold px-2 rounded-lg text-xl outline-none focus:outline-none  mx-1 "
                                                onClick={() => this.HandleClickDelete(data.addressUuid)}
                                            >
                                                <i className="fas fa-trash-alt mr-1" />
                                            </button>
                                        </TableCell>
                                    </TableRow>
                                </TableBody >
                            ))
                        ) : (
                            <TableBody>
                                <TableRow className="bg-white dark:bg-cool-gray-800 text-center">
                                    <TableCell colSpan="13" className=" text-gray-900 dark:text-white poppins-300 border-b   dark:border-white">
                                        {errorGetAllAddress ?
                                            <h1 className="text-red-500">{errorGetAllAddress}</h1>
                                            :
                                            <h1>Data Not Found</h1>
                                        }
                                    </TableCell>
                                </TableRow>
                            </TableBody>
                        )
                        :
                        <TableBody>
                            <TableRow className="bg-white dark:bg-cool-gray-800 text-center">
                                <TableCell colSpan="13" className=" text-gray-900 dark:text-white poppins-300 border-b   dark:border-white">
                                    Loading ... <ClipLoader size={15} color={"#000000"} />
                                </TableCell>
                            </TableRow>
                        </TableBody>
                    }
                </Table>
                <div className="pt-4">
                    <Pagination
                        totalResults={GetAllAddress.count}
                        resultsPerPage={10}
                        onChange={this.onPageChange.bind(this)}
                        label="Table Pagination" />
                </div>
            </>
        )
    }
}

export default connect(mapStateToProps)(AddressTable)