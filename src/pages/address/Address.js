import React, { Component } from 'react'
import { connect } from 'react-redux'
import { CreateAddress, GetAllAddress, UpdateAddress } from 'src/api/actions/address';
import AddresssFrom from './AddresssFrom';
import AddressTable from './AddressTable';
import Swal from 'sweetalert2'
import Maps from '../maps/Maps';
import { GetAllToko } from 'src/api/actions/toko';
import { ToastContainer } from 'react-toastify';

const mapStateToProps = (state) => {
  return {
    GetAllToko: state.Toko.GetAllToko,
    errorGetAllToko: state.Toko.errorGetAllToko,
    GetAllAddress: state.Address.GetAllAddress,
    errorGetAllAddress: state.Address.errorGetAllAddress,
    CreateAddress: state.Address.CreateAddress,
    errorCreateAddress: state.Address.errorCreateAddress,
    UpdateAddress: state.Address.UpdateAddress,
    errorUpdateAddress: state.Address.errorUpdateAddress,
    DeleteAddress: state.Address.DeleteAddress,
    errorDeleteAddress: state.Address.errorDeleteAddress,
    addressRoute: state.ReduxState.addressRoute,
    GetIdAddress: state.Address.GetIdAddress,
    errorGetIdAddress: state.Address.errorGetIdAddress,
    lat: state.ReduxState.lat,
    lng: state.ReduxState.lng,
  };
};

class Address extends Component {
  componentDidMount() {
    if (!this.props.GetAllToko) {
      this.props.dispatch(GetAllToko({ limit: 1000 }))
    }
    const tokoUuid = this.props.match.params.tokoUuid
    this.props.dispatch(GetAllAddress({ tokoUuid, limit: 1000 }))
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.errorGetAllAddress) {
      if (prevProps.errorGetAllAddress !== this.props.errorGetAllAddress) {
        this.props.history.replace('/toko')
      }
    }
    if (this.props.CreateAddress || this.props.UpdateAddress || this.props.DeleteAddress)
      if (prevProps.CreateAddress !== this.props.CreateAddress ||
        prevProps.UpdateAddress !== this.props.UpdateAddress ||
        prevProps.DeleteAddress !== this.props.DeleteAddress) {
        Swal.fire({
          text: (this.props.CreateAddress && this.props.CreateAddress.message) || (this.props.UpdateAddress && this.props.UpdateAddress.message) || (this.props.DeleteAddress && this.props.DeleteAddress.message),
          icon: "success",
        });
        this.props.dispatch({ type: "set", lat: false, lng: false })
        this.props.dispatch({ type: "CREATE_ADDRESS", payload: { data: false, errorMessage: false, }, })
        this.props.dispatch({ type: "UPDATE_ADDRESS", payload: { data: false, errorMessage: false, }, })
        this.props.dispatch({ type: "DELETE_ADDRESS", payload: { data: false, errorMessage: false, }, })
        this.props.dispatch(GetAllAddress({ tokoUuid: this.props.match.params.tokoUuid, limit: 1000 }))
      }
  }
  viewMaps() {
    this.props.dispatch(GetAllAddress({ tokoUuid: this.props.match.params.tokoUuid, limit: 1000 }))
    this.props.dispatch({ type: "set", showModal: true, mapsRoute: "getAll" })
  }
  handleSubmit(data) {
    const dataReq = Object.assign({}, data, { type: Number(data.type), status: Number(data.status), tokoUuid: this.props.match.params.tokoUuid, latitude: String(this.props.lat), longitude: String(this.props.lng) })
    if (this.props.addressRoute === "create") {
      this.props.dispatch(CreateAddress(dataReq))
    } else {
      this.props.dispatch(UpdateAddress(this.props.GetIdAddress.addressUuid, dataReq))
    }
    this.props.dispatch({ type: "set", addressRoute: "table" })
    data = ""
  }
  render() {
    const { GetAllToko, addressRoute } = this.props
    console.log(this.props.CreateAddress, "this.props.CreateAddress")
    return (
      <>
        {addressRoute === "table" ?
          <>
          <ToastContainer position="top-right" />
            <Maps />
            <div className='flex justify-around mt-2 pb-4 capitalize'>
              <div className=' borderBox p-1 px-4 rounded-lg cursor-pointer bg-blue-500 text-white'
                onClick={() => this.props.history.replace('/toko')}
              >
                <i className="fas fa-arrow-left mr-2"></i>  Back
              </div>
              <u className='capitalize'>
                <div className='text-center poppins-600 bold capitalize'>
                  Toko {GetAllToko && GetAllToko.rows.filter(i => i.tokoUuid === this.props.match.params.tokoUuid).map(i => i.name)}
                </div>
              </u>
              <div className='text-center poppins-600 bold capitalize'>
                <button className="poppins-500 bg-sidebar py-1 px-2 rounded-lg border borderBox dark:bg-gray-400 dark:border-white  hover:border-white hover:shadow-none hover:bg-cool-gray-300 dark:hover:bg-cool-gray-400"
                  onClick={() => this.props.dispatch({ type: "set", addressRoute: "create" })}
                >
                  Add New Address
                </button>
              </div>
              <div className='text-center poppins-600 bold capitalize'>
                <button className="ml-4 poppins-500 bg-sidebar py-1 px-2 rounded-lg border borderBox dark:bg-gray-400 dark:border-white  hover:border-white hover:shadow-none hover:bg-cool-gray-300 dark:hover:bg-cool-gray-400"
                  onClick={() => this.viewMaps()}
                >
                  View Maps
                </button>
              </div>
            </div>
            <AddressTable />
          </> :
          <AddresssFrom onSubmit={data => this.handleSubmit(data)} />
        }
      </>
    )
  }
}

export default connect(mapStateToProps)(Address)