import React, { useEffect, useState } from 'react'
import { Modal, ModalBody, ModalFooter, Button } from '@windmill/react-ui'
import { GoogleMap, Marker, InfoWindow } from "@react-google-maps/api";
import { getGeocode, getLatLng } from "use-places-autocomplete";
import { useDispatch, useSelector } from 'react-redux';


function Maps(props) {
    const dispatch = useDispatch()
    const [center, setCenter] = useState(false)
    const [dataDetail, setdataDetail] = useState(false)
    const [selected, setSelected] = useState(false)
    const getAllType = useSelector((state) => state.Type.getAllType)
    const GetAllToko = useSelector((state) => state.Toko.GetAllToko)
    const GetIdAddress = useSelector((state) => state.Address.GetIdAddress)
    const GetAllAddress = useSelector((state) => state.Address.GetAllAddress)
    const GetIdToko = useSelector((state) => state.Toko.GetIdToko)
    const mapsRoute = useSelector((state) => state.ReduxState.mapsRoute)
    const tokoRoute = useSelector((state) => state.ReduxState.tokoRoute)
    const addressRoute = useSelector((state) => state.ReduxState.addressRoute)
    const showModal = useSelector((state) => state.ReduxState.showModal)
    const Cancel = () => {
        if (GetIdToko) {
            dispatch({
                type: "GET_ID_TOKO", payload: { data: false, errorMessage: false, },
            })
        }
        dispatch({
            type: "set", showModal: false,
            tokoRoute: tokoRoute === "create" ? tokoRoute : "table",
            addressRoute: addressRoute === "create" ? addressRoute : "table",
            mapsRoute: false
        })
    }

    useEffect(() => {
        if (props.valueAlamat !== "" && (tokoRoute === "create" || addressRoute === "create")) {
            const parameter = {
                address: `${props.valueAlamat}`
            }
            getGeocode(parameter).then((results) => {
                const { lat, lng } = getLatLng(results[0]);
                setCenter({ lat, lng })
                dispatch({ type: "set", lat: lat, lng: lng })

            });
        }
    }, [])

    const containerStyle = {
        width: '530px',
        height: '400px'
    };

    const onLoad = marker => {
        console.log('marker: ', marker)
    }
    const onDraggableChanged = e => {
        const lat = e.latLng.lat();
        const lng = e.latLng.lng();
        setCenter({ lat, lng })
        dispatch({ type: "set", lat: lat, lng: lng })
    }
    function details(data) {
        setCenter({ lat: Number(data.latitude), lng: Number(data.longitude) })
        setdataDetail(data)
        setSelected(true)
    }
    return (
        <>
            <Modal isOpen={showModal} onClose={() => Cancel()} >
                {/* <ModalHeader className='text-center'> Search Location </ModalHeader> */}
                <ModalBody>
                    {center &&
                        (tokoRoute === "create" || addressRoute === "create") ?
                        <GoogleMap
                            mapContainerStyle={containerStyle}
                            center={center}
                            zoom={18}
                        >
                            <Marker
                                onLoad={onLoad}
                                position={center}
                                draggable={true}
                                label={props.valueName}
                                zIndex={10}
                                onDragEnd={(e) => onDraggableChanged(e)}
                            />
                        </GoogleMap> :
                        GetIdToko && mapsRoute === "getOne" ?
                            <GoogleMap
                                mapContainerStyle={containerStyle}
                                center={GetIdToko && GetIdToko.addresses.filter(i => i.type === 1).length ? GetIdToko.addresses.map(i => { return { lat: Number(i.latitude), lng: Number(i.longitude) } })[0] : GetIdToko && { lat: Number(GetIdToko.addresses[0].latitude), lng: Number(GetIdToko.addresses[0].longitude) }}
                                zoom={18}
                            >
                                {GetIdToko && GetIdToko.addresses.map((data, id) => (
                                    <Marker
                                        key={id}
                                        label={`${id + 1}`}
                                        onLoad={onLoad}
                                        position={{ lat: Number(data.latitude), lng: Number(data.longitude) }}
                                        title={GetIdToko.name}
                                        onClick={() => details(data)}
                                    />
                                ))}
                                {selected && GetIdToko && (
                                    <InfoWindow
                                        position={center}
                                        onCloseClick={() => { setSelected(false) }}
                                    >
                                        <div style={{ maxWidth: "12rem" }}>
                                            <h2 className='poppins-800 bold capitalize'>Toko {GetIdToko.name} ( {dataDetail && getAllType && getAllType.filter(i => i.id === dataDetail.type).map(i => i.name)} )</h2>
                                            <h3 className='poppins-300 bold'>{dataDetail.phone}</h3>
                                            <div className='max-w-xs'>{dataDetail.address}</div>
                                            <div className='max-w-xs'>{dataDetail.zip_code}, Indonesia</div>
                                            <div></div>
                                        </div>
                                    </InfoWindow>
                                )}
                            </GoogleMap>
                            :
                            GetIdAddress && mapsRoute === "getOne" ?
                                <GoogleMap
                                    mapContainerStyle={containerStyle}
                                    center={GetIdAddress && { lat: Number(GetIdAddress.latitude), lng: Number(GetIdAddress.longitude) }}
                                    zoom={18}
                                >
                                    <Marker
                                        label={`${GetIdAddress.id}`}
                                        onLoad={onLoad}
                                        position={{ lat: Number(GetIdAddress.latitude), lng: Number(GetIdAddress.longitude) }}
                                        title={GetIdAddress.name}
                                        onClick={() => details(GetIdAddress)}
                                    />

                                    {selected && GetIdAddress && (
                                        <InfoWindow
                                            position={center}
                                            onCloseClick={() => { setSelected(false) }}
                                        >
                                            <div style={{ maxWidth: "12rem" }}>
                                                <h2 className='poppins-800 bold capitalize'>Toko {dataDetail && GetAllToko && GetAllToko.rows.filter(i => i.tokoUuid === dataDetail.tokoUuid).map(i => i.name)} ( {dataDetail && getAllType && getAllType.filter(i => i.id === dataDetail.type).map(i => i.name)} )</h2>
                                                <h3 className='poppins-300 bold'>{dataDetail.phone}</h3>
                                                <div className='max-w-xs'>{dataDetail.address}</div>
                                                <div className='max-w-xs'>{dataDetail.zip_code}, Indonesia</div>
                                                <div></div>
                                            </div>
                                        </InfoWindow>
                                    )}
                                </GoogleMap> :
                                GetAllAddress && mapsRoute === "getAll" &&
                                GetAllAddress.rows.length &&
                                <GoogleMap
                                    mapContainerStyle={containerStyle}
                                    center={GetAllAddress && { lat: Number(GetAllAddress.rows[0].latitude), lng: Number(GetAllAddress.rows[0].longitude) }}
                                    zoom={18}
                                >
                                    {GetAllAddress.rows.map((data, id) => (
                                        <Marker
                                            key={id}
                                            label={`${id + 1}`}
                                            onLoad={onLoad}
                                            position={{ lat: Number(data.latitude), lng: Number(data.longitude) }}
                                            title={data.name}
                                            onClick={() => details(data)}
                                        />
                                    ))}
                                    {selected && GetAllAddress && (
                                        <InfoWindow
                                            position={center}
                                            onCloseClick={() => { setSelected(false) }}
                                        >
                                            <div style={{ maxWidth: "12rem" }}>
                                                <h2 className='poppins-800 bold capitalize'>Toko {dataDetail && GetAllToko && GetAllToko.rows.filter(i => i.tokoUuid === dataDetail.tokoUuid).map(i => i.name)} ( {dataDetail && getAllType && getAllType.filter(i => i.id === dataDetail.type).map(i => i.name)} )</h2>
                                                <h3 className='poppins-300 bold'>{dataDetail.phone}</h3>
                                                <div className='max-w-xs'>{dataDetail.address}</div>
                                                <div className='max-w-xs'>{dataDetail.zip_code}, Indonesia</div>
                                                <div></div>
                                            </div>
                                        </InfoWindow>
                                    )}
                                </GoogleMap>
                    }
                </ModalBody>
                <ModalFooter>
                    {(tokoRoute === "create" || addressRoute === "create") ? <>
                        <Button className="w-full sm:w-auto" layout="outline" onClick={() => Cancel()}>
                            Cancel
                        </Button>
                        <Button className="w-full sm:w-auto" onClick={() => Cancel()}>
                            SAVE
                        </Button>
                    </> :
                        <Button className="w-full sm:w-auto" layout="outline" onClick={() => Cancel()}>
                            Close
                        </Button>
                    }
                </ModalFooter>
            </Modal>
        </>
    )
}

export default Maps