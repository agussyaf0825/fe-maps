
import {
    GET_ALL_STATUS,
} from "src/api/actions/status"
let initialState = {
    getAllStatus: false,
    errorGetAllStatus: false,
}

export const Status = (state = initialState, action) => {
    switch (action.type) {
        case GET_ALL_STATUS:
            return {
                ...state,
                getAllStatus: action.payload.data,
                errorGetAllStatus: action.payload.errorMessage,
            };
        default:
            return state;
    }
}