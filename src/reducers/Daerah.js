
import {
    GET_ALL_KECAMATAN,
    GET_ID_KECAMATAN,
} from "src/api/actions/daerah/kecamatan"
import {
    GET_ALL_KELURAHAN,
    GET_ID_KELURAHAN,
} from "src/api/actions/daerah/kelurahan"
import {
    GET_ALL_KOTA_KABUPATEN,
    GET_ID_KOTA_KABUPATEN,
} from "src/api/actions/daerah/kotaKabupaten"
import {
    GET_ALL_PROVINSI,
    GET_ID_PROVINSI,
} from "src/api/actions/daerah/provinsi"

let initialState = {
    GetAllKecamatan: false,
    errorGetAllKecamatan: false,
    GetIdKecamatan: false,
    errorGetIdKecamatan: false,
    GetAllKelurahan: false,
    errorGetAllKelurahan: false,
    GetIdKelurahan: false,
    errorGetIdKelurahan: false,
    GetAllKotaKabupaten: false,
    errorGetAllKotaKabupaten: false,
    GetIdKotaKabupaten: false,
    errorGetIdKotaKabupaten: false,
    GetAllProvinsi: false,
    errorGetAllProvinsi: false,
    GetIdProvinsi: false,
    errorGetIdProvinsi: false,
}

export const Daerah = (state = initialState, action) => {
    switch (action.type) {
        case GET_ALL_KECAMATAN:
            return {
                ...state,
                GetAllKecamatan: action.payload.data,
                errorGetAllKecamatan: action.payload.errorMessage,
            };
        case GET_ID_KECAMATAN:
            return {
                ...state,
                GetIdKecamatan: action.payload.data,
                errorGetIdKecamatan: action.payload.errorMessage,
            };
        case GET_ALL_KELURAHAN:
            return {
                ...state,
                GetAllKelurahan: action.payload.data,
                errorGetAllKelurahan: action.payload.errorMessage,
            };
        case GET_ID_KELURAHAN:
            return {
                ...state,
                GetIdKelurahan: action.payload.data,
                errorGetIdKelurahan: action.payload.errorMessage,
            };
        case GET_ALL_KOTA_KABUPATEN:
            return {
                ...state,
                GetAllKotaKabupaten: action.payload.data,
                errorGetAllKotaKabupaten: action.payload.errorMessage,
            };
        case GET_ID_KOTA_KABUPATEN:
            return {
                ...state,
                GetIdKotaKabupaten: action.payload.data,
                errorGetIdKotaKabupaten: action.payload.errorMessage,
            };
        case GET_ALL_PROVINSI:
            return {
                ...state,
                GetAllProvinsi: action.payload.data,
                errorGetAllProvinsi: action.payload.errorMessage,
            };
        case GET_ID_PROVINSI:
            return {
                ...state,
                GetIdProvinsi: action.payload.data,
                errorGetIdProvinsi: action.payload.errorMessage,
            };
        default:
            return state;
    }
}