
import {
    CREATE_ADDRESS,
    DELETE_ADDRESS,
    GET_ALL_ADDRESS,
    GET_ID_ADDRESS,
    UPDATE_ADDRESS,
} from "src/api/actions/address"
let initialState = {
    CreateAddress: false,
    errorCreateAddress: false,
    DeleteAddress: false,
    errorDeleteAddress: false,
    GetAllAddress: false,
    errorGetAllAddress: false,
    GetIdAddress: false,
    errorGetIdAddress: false,
    UpdateAddress: false,
    errorUpdateAddress: false,
}

export const Address = (state = initialState, action) => {
    switch (action.type) {
        case CREATE_ADDRESS:
            return {
                ...state,
                CreateAddress: action.payload.data,
                errorCreateAddress: action.payload.errorMessage,
            };
        case DELETE_ADDRESS:
            return {
                ...state,
                DeleteAddress: action.payload.data,
                errorDeleteAddress: action.payload.errorMessage,
            };
        case GET_ALL_ADDRESS:
            return {
                ...state,
                GetAllAddress: action.payload.data,
                errorGetAllAddress: action.payload.errorMessage,
            };
        case GET_ID_ADDRESS:
            return {
                ...state,
                GetIdAddress: action.payload.data,
                errorGetIdAddress: action.payload.errorMessage,
            };
        case UPDATE_ADDRESS:
            return {
                ...state,
                UpdateAddress: action.payload.data,
                errorUpdateAddress: action.payload.errorMessage,
            };
        default:
            return state;
    }
}