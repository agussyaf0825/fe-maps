
import {
    GET_ALL_TYPE,
} from "src/api/actions/type"
let initialState = {
    getAllType: false,
    errorGetAllType: false,
}

export const Type = (state = initialState, action) => {
    switch (action.type) {
        case GET_ALL_TYPE:
            return {
                ...state,
                getAllType: action.payload.data,
                errorGetAllType: action.payload.errorMessage,
            };
        default:
            return state;
    }
}