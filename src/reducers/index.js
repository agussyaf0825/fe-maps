import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import ReduxState from "./ReduxState";
import { Address } from './Address'
import { Toko } from './Toko'
import { Daerah } from './Daerah'
import { Status } from "./Status";
import { Type } from "./Type";

export default combineReducers({
    Daerah,
    Type,
    Status,
    Address,
    Toko,
    ReduxState,
    form: formReducer,
});