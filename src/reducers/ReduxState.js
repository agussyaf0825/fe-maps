const initialState = {
    modal: true,
    tokoRoute: "table",
    addressRoute: "table",
    tokoEdit: false,
    mapsRoute: false,
    sidebarOpen: false,
    ProfileMenuOpen: false,
    DropdownMenuOpen: false,
    Mode: false,
    loading: false,
    disable: false,
    showModal: false,
    btnDropDown: 10,
    btnDropDownShow: false,
    changePassword: false,
    shidebarMinize: false,
    menuDesktop: true,
    lat: "",
    lng: "",
};

const ReduxState = (state = initialState, { type, ...rest }) => {
    switch (type) {
        case "set":
            return { ...state, ...rest };
        default:
            return state;
    }
};
export default ReduxState;
