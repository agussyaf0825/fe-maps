
import {
    CREATE_TOKO,
    DELETE_TOKO,
    GET_ALL_TOKO,
    GET_ID_TOKO,
    UPDATE_TOKO,
} from "src/api/actions/toko"
let initialState = {
    CreateToko: false,
    errorCreateToko: false,
    DeleteToko: false,
    errorDeleteToko: false,
    GetAllToko: false,
    errorGetAllToko: false,
    GetIdToko: false,
    errorGetIdToko: false,
    UpdateToko: false,
    errorUpdateToko: false,
}

export const Toko = (state = initialState, action) => {
    switch (action.type) {
        case CREATE_TOKO:
            return {
                ...state,
                CreateToko: action.payload.data,
                errorCreateToko: action.payload.errorMessage,
            };
        case DELETE_TOKO:
            return {
                ...state,
                DeleteToko: action.payload.data,
                errorDeleteToko: action.payload.errorMessage,
            };
        case GET_ALL_TOKO:
            return {
                ...state,
                GetAllToko: action.payload.data,
                errorGetAllToko: action.payload.errorMessage,
            };
        case GET_ID_TOKO:
            return {
                ...state,
                GetIdToko: action.payload.data,
                errorGetIdToko: action.payload.errorMessage,
            };
        case UPDATE_TOKO:
            return {
                ...state,
                UpdateToko: action.payload.data,
                errorUpdateToko: action.payload.errorMessage,
            };
        default:
            return state;
    }
}