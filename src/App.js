import React, { Component } from 'react';
import {
  HashRouter,
  // BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';

const loading = (
  <div className="text-xl text-center">
    <div className="animate-spin"></div>
  </div>
)

const Layout = React.lazy(() => import('./Layout/Layout'));

class App extends Component {
  componentDidMount() {

  }
  render() {
    return (
      <HashRouter>
        <React.Suspense fallback={loading}>
          <Switch>
            <Route path="/" name="Home" component={Layout} />
          </Switch>
        </React.Suspense>
      </HashRouter>
    );
  }
}

export default App;
