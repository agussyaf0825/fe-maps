
import React from 'react';

const Toko = React.lazy(() => import('./pages/toko/Toko'));
const TokoCreateNew = React.lazy(() => import('./pages/toko/TokoCreateNew'));


const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/toko', exact: true, name: 'Toko', component: Toko },
  { path: '/toko/add-new', name: 'Toko Add New', component: TokoCreateNew },

]

export default routes;