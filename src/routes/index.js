
import React from 'react';

const Toko = React.lazy(() => import('../pages/toko/Toko'));
const Address = React.lazy(() => import('../pages/address/Address'));


const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/toko', exact: true, name: 'Toko', component: Toko },
  { path: '/toko/address/:tokoUuid', name: 'Toko Address Detail', component: Address },

]

export default routes;