import React from "react";
import Select from "react-select";
export const FieldFormInput = ({
  input,
  label,
  type,
  defaultValue,
  placeholder,
  disabled,
  autoComplete,
  readOnly,
  maxLength,
  minLength,
  showModal,
  validasi,
  meta: { touched, error, warning },
}) => (
  <>
    <div className="wrapper">
      <label className="pl-3 pb-2 block text-blueGray-600 text-xs font-bold dark:text-white capitalize">
        {label} {validasi && <span className="text-red-500 ml-1 poppins-800 ">*</span>}
      </label>
      <input
        {...input}
        type={type}
        defaultValue={defaultValue}
        autoComplete={autoComplete}
        className={
          (showModal ? "dark:bg-gray-800 " : "dark:bg-cool-gray-800 ") +
          " px-3 placeholder-blueGray-300 text-sm w-full focus:outline-none focus:ring-0  border-0 border-b-2 focus:border-0 dark:text-white dark:placeholder-cool-gray-200"
        }
        placeholder={placeholder}
        disabled={disabled}
        readOnly={readOnly}
        maxLength={maxLength}
        minLength={minLength}
      />
      {touched &&
        ((error && (
          <span className="text-sm text-red-500 capitalize">{error}</span>
        )) ||
          (warning && (
            <span className="text-sm text-yellow-500">{warning}</span>
          )))}
    </div>
  </>
);

export const FieldFormInputAnim = ({
  input,
  label,
  type,
  name,
  defaultValue,
  placeholder,
  disabled,
  autoComplete,
  readOnly,
  maxLength,
  minLength,
  validasi,
  meta: { touched, error, warning },
}) => (
  <>
    <div className="relative w-full mb-3 ">
      <input
        {...input}
        type={type}
        name={name}
        defaultValue={defaultValue}
        autoComplete={autoComplete}
        className="px-3 py-3 placeholder-blueGray-300 text-sm w-full focus:outline-none focus:ring-0  border-0 border-b-2 focus:border-0 Iinput dark:placeholder-cool-gray-200"
        placeholder={placeholder}
        disabled={disabled}
        readOnly={readOnly}
        maxLength={maxLength}
        minLength={minLength}
      />
      <label
        htmlFor={name}
        className="absolute top-3 -z-1  left-0 duration-300 origin-0"
      >
        {label} {validasi && <span className="text-red-500 ml-1 poppins-800 ">*</span>}
      </label>
      <div className="fixed">
        {touched &&
          ((error && (
            <span className="text-sm text-red-500 capitalize">{error}</span>
          )) ||
            (warning && (
              <span className="text-sm text-yellow-500">{warning}</span>
            )))}
      </div>
    </div>
  </>
);

export const FieldFormInputSelect = ({
  input,
  label,
  meta: { touched, error, warning },
  children,
  disabled,
  readOnly,
  showModal,
  validasi,
  ...custom
}) => (
  <>
    <div className="wrapper">
      <label className="pl-3 pb-2 block text-blueGray-600 text-xs font-bold dark:text-white capitalize">
        {label} {validasi && <span className="text-red-500 ml-1 poppins-800 ">*</span>}
      </label>
      <select
        className={
          (showModal ? "dark:bg-gray-800" : "dark:bg-cool-gray-800 ") +
          " px-3 py-2 placeholder-blueGray-300 text-sm w-full focus:outline-none focus:ring-0 border-0 border-b-2 focus:border-0  dark:text-white dark:placeholder-cool-gray-200 capitalize"
        }
        {...input}
        {...custom}
        disabled={disabled}
        readOnly={readOnly}
      >
        {children}
      </select>
      {touched &&
        ((error && (
          <span className="text-sm text-red-500 capitalize">{error}</span>
        )) ||
          (warning && (
            <span className="text-sm text-yellow-500">{warning}</span>
          )))}
    </div>
  </>
);

export const FieldFormInputSpan = ({
  input,
  label,
  type,
  defaultValue,
  placeholder,
  disabled,
  autoComplete,
  readOnly,
  maxLength,
  minLength,
  validasi,
  meta: { touched, error, warning },
}) => (
  <>
    <div className="wrapper">
    <label className="pl-3 pb-2 block text-blueGray-600 text-xs font-bold dark:text-white capitalize">
        {label} {validasi && <span className="text-red-500 ml-1 poppins-800 ">*</span>}
      </label>
      <textarea
        {...input}
        type={type}
        defaultValue={defaultValue}
        autoComplete={autoComplete}
        className="px-3 py-3 placeholder-blueGray-300 text-sm w-full focus:outline-none focus:ring-0  border-0 border-b-2 focus:border-0 dark:bg-cool-gray-800  dark:placeholder-cool-gray-200"
        placeholder={placeholder}
        disabled={disabled}
        readOnly={readOnly}
        maxLength={maxLength}
        minLength={minLength}
      />
      {touched &&
        ((error && (
          <span className="text-sm text-red-500 capitalize">{error}</span>
        )) ||
          (warning && (
            <span className="text-sm text-yellow-500">{warning}</span>
          )))}
    </div>
  </>
);


export const FieldFormInputCheckBox = ({
  input,
  label,
  type,
  // defaultValue,
  placeholder,
  disabled,
  // autoComplete,
  readOnly,
  // maxLength,
  // minLength,
  showModal,
  meta: { touched, error, warning },
}) => (
  <>
    <div className="wrapper">
      <label className="block text-blueGray-600 text-xs font-bold dark:text-white capitalize">
        {label}
      </label>
      <input
        {...input}
        type={type}
        className={showModal ? "dark:bg-gray-800 " : "dark:bg-cool-gray-800 "}
        // className={(showModal ? "dark:bg-gray-800 " : "dark:bg-cool-gray-800 ") + " px-3 placeholder-blueGray-300 text-sm w-full focus:outline-none focus:ring-0  border-0 border-b-2 focus:border-0 dark:text-white dark:placeholder-cool-gray-200"}
        placeholder={placeholder}
        disabled={disabled}
        readOnly={readOnly}
      />
      {touched &&
        ((error && (
          <span className="text-sm text-red-500 capitalize">{error}</span>
        )) ||
          (warning && (
            <span className="text-sm text-yellow-500">{warning}</span>
          )))}
    </div>
  </>
);

export const FieldFormInputDefault = ({
  input,
  type,
  defaultValue,
  placeholder,
  disabled,
  autoComplete,
  readOnly,
  maxLength,
  minLength,
  showModal,
  meta: { touched, error, warning },
}) => (
  <>
    <div className="wrapper">
      <input
        {...input}
        type={type}
        defaultValue={defaultValue}
        autoComplete={autoComplete}
        className={
          (showModal ? "dark:bg-gray-800 " : "dark:bg-cool-gray-800 ") +
          "  w-full px-3 placeholder-blueGray-300 text-xs  focus:outline-none  dark:text-white dark:placeholder-cool-gray-200"
        }
        placeholder={placeholder}
        disabled={disabled}
        readOnly={readOnly}
        maxLength={maxLength}
        minLength={minLength}
      />
      {touched &&
        ((error && (
          <span className="text-sm text-red-500 capitalize">{error}</span>
        )) ||
          (warning && (
            <span className="text-sm text-yellow-500">{warning}</span>
          )))}
    </div>
  </>
);

export const FieldFormInputSelectDefault = ({
  input,
  label,
  meta: { touched, error, warning },
  children,
  disabled,
  readOnly,
  showModal,
  ...custom
}) => (
  <>
    <div className="wrapper">
      <select
        className={
          (showModal ? "dark:bg-gray-800" : "dark:bg-cool-gray-800 ") +
          " px-3  placeholder-blueGray-300 text-sm w-full focus:outline-none  capitalize dark:text-white dark:placeholder-cool-gray-200"
        }
        {...input}
        {...custom}
        disabled={disabled}
        readOnly={readOnly}
      >
        {children}
      </select>
      {touched &&
        ((error && (
          <span className="text-sm text-red-500 capitalize">{error}</span>
        )) ||
          (warning && (
            <span className="text-sm text-yellow-500">{warning}</span>
          )))}
    </div>
  </>
);
export const FieldFormInputMultiSelect = ({
  input,
  label,
  defaultValue,
  isMulti,
  name,
  placeholder,
  options,
  meta: { touched, error, warning },
  ...custom
}) => (
  <>
    <div className="wrapper">
      <Select
        {...input}
        {...custom}
        defaultValue={defaultValue}
        isMulti={isMulti}
        name={name}
        placeholder={placeholder}
        options={options}
      />
      {touched &&
        ((error && (
          <span className="text-sm text-red-500 capitalize">{error}</span>
        )) ||
          (warning && (
            <span className="text-sm text-yellow-500">{warning}</span>
          )))}
    </div>
  </>
);
