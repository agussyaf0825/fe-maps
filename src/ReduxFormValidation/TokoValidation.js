const TokoValidation = (values) => {
    const errors = {};
    if (!values.name || values.name === "") {
        errors.name = "Please enter your Name Toko";
    }
    if (!values.status || values.status === "") {
        errors.status = "Please your select Status Toko";
    }
    if(!values.address || values.address === ""){
        errors.address = "Please enter your Alamat Lengkap"
    }
    if(!values.nameLengkap || values.nameLengkap === ""){
        errors.nameLengkap = "Please enter your Full Name"
    }
    if(!values.phone || values.phone === ""){
        errors.phone = "Please enter your Phone Number"
    }
    if(!values.provinsi_id || values.provinsi_id === ""){
        errors.provinsi_id = "Please your select Provinsi"
    }
    if(!values.kota_kabupaten_id || values.kota_kabupaten_id === ""){
        errors.kota_kabupaten_id = "Please your select Kota / Kabupaten"
    }
    if(!values.kecamatan_id || values.kecamatan_id === ""){
        errors.kecamatan_id = "Please your select Kecamatan"
    }
    if(!values.kelurahan_id || values.kelurahan_id === ""){
        errors.kelurahan_id = "Please your select Kelurahan"
    }
    if(!values.type || values.type === ""){
        errors.type = "Please your select Type Address"
    }
    if(!values.status_alamat || values.status_alamat === ""){
        errors.status_alamat = "Please your select Status Address"
    }

    return errors
}

export default TokoValidation