import daerahRouter from "src/api/urlRoute/Daerah";

export const GET_ID_KECAMATAN = "GET_ID_KECAMATAN"

// ==================================== Get Id Kecamatan ======================================//
export const GetIdKecamatan = (id) => {
    return async (dispatch) => {
        return await daerahRouter.GetIdKecamatan(id)
            .then((response) => {
                dispatch({
                    type: GET_ID_KECAMATAN,
                    payload: {
                        data: response.data,
                        errorMessage: false,
                    },
                });
            })
            .catch((error) => {
                if (error.response && error.response.data) {
                    dispatch({
                        type: GET_ID_KECAMATAN,
                        payload: {
                            data: false,
                            errorMessage: error.response.data.message,
                        },
                    });
                } else {
                    dispatch({
                        type: GET_ID_KECAMATAN,
                        payload: {
                            data: false,
                            errorMessage: error.message,
                        },
                    });
                }
            });
    };
}
// ==================================== End Get Id Kecamatan ======================================//