import daerahRouter from "src/api/urlRoute/Daerah";

export const GET_ALL_KECAMATAN = "GET_ALL_KECAMATAN"

// ==================================== Get All Kecamatan ======================================//
export const GetAllKecamatan = (params) => {
    return async (dispatch) => {
        return await daerahRouter.GetAllKecamatan(params)
            .then((response) => {
                dispatch({
                    type: GET_ALL_KECAMATAN,
                    payload: {
                        data: response.data,
                        errorMessage: false,
                    },
                });
            })
            .catch((error) => {
                if (error.response && error.response.data) {
                    dispatch({
                        type: GET_ALL_KECAMATAN,
                        payload: {
                            data: false,
                            errorMessage: error.response.data.message,
                        },
                    });
                } else {
                    dispatch({
                        type: GET_ALL_KECAMATAN,
                        payload: {
                            data: false,
                            errorMessage: error.message,
                        },
                    });
                }
            });
    };
}
// ==================================== End Get All Kecamatan ======================================//