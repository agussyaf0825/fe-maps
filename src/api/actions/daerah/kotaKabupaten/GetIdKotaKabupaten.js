import daerahRouter from "src/api/urlRoute/Daerah";

export const GET_ID_KOTA_KABUPATEN = "GET_ID_KOTA_KABUPATEN"

// ==================================== Get Id Kota Kabupaten ======================================//
export const GetIdKotaKabupaten = (id) => {
    return async (dispatch) => {
        return await daerahRouter.GetIdKotaKabupaten(id)
            .then((response) => {
                dispatch({
                    type: GET_ID_KOTA_KABUPATEN,
                    payload: {
                        data: response.data,
                        errorMessage: false,
                    },
                });
            })
            .catch((error) => {
                if (error.response && error.response.data) {
                    dispatch({
                        type: GET_ID_KOTA_KABUPATEN,
                        payload: {
                            data: false,
                            errorMessage: error.response.data.message,
                        },
                    });
                } else {
                    dispatch({
                        type: GET_ID_KOTA_KABUPATEN,
                        payload: {
                            data: false,
                            errorMessage: error.message,
                        },
                    });
                }
            });
    };
}
// ==================================== End Get Id Kota Kabupaten ======================================//