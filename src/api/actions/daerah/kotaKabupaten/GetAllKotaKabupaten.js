import daerahRouter from "src/api/urlRoute/Daerah";

export const GET_ALL_KOTA_KABUPATEN = "GET_ALL_KOTA_KABUPATEN"

// ==================================== Get All Kota Kabupaten ======================================//
export const GetAllKotaKabupaten = (params) => {
    return async (dispatch) => {
        return await daerahRouter.GetAllKotaKabupaten(params)
            .then((response) => {
                dispatch({
                    type: GET_ALL_KOTA_KABUPATEN,
                    payload: {
                        data: response.data,
                        errorMessage: false,
                    },
                });
            })
            .catch((error) => {
                if (error.response && error.response.data) {
                    dispatch({
                        type: GET_ALL_KOTA_KABUPATEN,
                        payload: {
                            data: false,
                            errorMessage: error.response.data.message,
                        },
                    });
                } else {
                    dispatch({
                        type: GET_ALL_KOTA_KABUPATEN,
                        payload: {
                            data: false,
                            errorMessage: error.message,
                        },
                    });
                }
            });
    };
}
// ==================================== End Get All Kota Kabupaten ======================================//