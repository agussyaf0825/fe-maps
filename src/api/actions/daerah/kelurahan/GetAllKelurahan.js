import daerahRouter from "src/api/urlRoute/Daerah";

export const GET_ALL_KELURAHAN = "GET_ALL_KELURAHAN"

// ==================================== Get All Kelurahan ======================================//
export const GetAllKelurahan = (params) => {
    return async (dispatch) => {
        return await daerahRouter.GetAllKelurahan(params)
            .then((response) => {
                dispatch({
                    type: GET_ALL_KELURAHAN,
                    payload: {
                        data: response.data,
                        errorMessage: false,
                    },
                });
            })
            .catch((error) => {
                if (error.response && error.response.data) {
                    dispatch({
                        type: GET_ALL_KELURAHAN,
                        payload: {
                            data: false,
                            errorMessage: error.response.data.message,
                        },
                    });
                } else {
                    dispatch({
                        type: GET_ALL_KELURAHAN,
                        payload: {
                            data: false,
                            errorMessage: error.message,
                        },
                    });
                }
            });
    };
}
// ==================================== End Get All Kelurahan ======================================//