import daerahRouter from "src/api/urlRoute/Daerah";

export const GET_ID_KELURAHAN = "GET_ID_KELURAHAN"

// ==================================== Get Id Kelurahan ======================================//
export const GetIdKelurahan = (id) => {
    return async (dispatch) => {
        return await daerahRouter.GetIdKelurahan(id)
            .then((response) => {
                dispatch({
                    type: GET_ID_KELURAHAN,
                    payload: {
                        data: response.data,
                        errorMessage: false,
                    },
                });
            })
            .catch((error) => {
                if (error.response && error.response.data) {
                    dispatch({
                        type: GET_ID_KELURAHAN,
                        payload: {
                            data: false,
                            errorMessage: error.response.data.message,
                        },
                    });
                } else {
                    dispatch({
                        type: GET_ID_KELURAHAN,
                        payload: {
                            data: false,
                            errorMessage: error.message,
                        },
                    });
                }
            });
    };
}
// ==================================== End Get Id Kelurahan ======================================//