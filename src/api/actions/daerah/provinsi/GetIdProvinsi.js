import daerahRouter from "src/api/urlRoute/Daerah";

export const GET_ID_PROVINSI = "GET_ID_PROVINSI"

// ==================================== Get Id Provinsi ======================================//
export const GetIdProvinsi = (id) => {
    return async (dispatch) => {
        return await daerahRouter.GetIdProvinsi(id)
            .then((response) => {
                dispatch({
                    type: GET_ID_PROVINSI,
                    payload: {
                        data: response.data,
                        errorMessage: false,
                    },
                });
            })
            .catch((error) => {
                if (error.response && error.response.data) {
                    dispatch({
                        type: GET_ID_PROVINSI,
                        payload: {
                            data: false,
                            errorMessage: error.response.data.message,
                        },
                    });
                } else {
                    dispatch({
                        type: GET_ID_PROVINSI,
                        payload: {
                            data: false,
                            errorMessage: error.message,
                        },
                    });
                }
            });
    };
}
// ==================================== End Get Id Provinsi ======================================//