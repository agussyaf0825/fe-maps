import daerahRouter from "src/api/urlRoute/Daerah";

export const GET_ALL_PROVINSI = "GET_ALL_PROVINSI"

// ==================================== Get All Provinsi ======================================//
export const GetAllProvinsi = (params) => {
    return async (dispatch) => {
        return await daerahRouter.GetAllProvinsi(params)
            .then((response) => {
                dispatch({
                    type: GET_ALL_PROVINSI,
                    payload: {
                        data: response.data,
                        errorMessage: false,
                    },
                });
            })
            .catch((error) => {
                if (error.response && error.response.data) {
                    dispatch({
                        type: GET_ALL_PROVINSI,
                        payload: {
                            data: false,
                            errorMessage: error.response.data.message,
                        },
                    });
                } else {
                    dispatch({
                        type: GET_ALL_PROVINSI,
                        payload: {
                            data: false,
                            errorMessage: error.message,
                        },
                    });
                }
            });
    };
}
// ==================================== End Get All Provinsi ======================================//