import addressRouter from "src/api/urlRoute/Address";

export const GET_ALL_ADDRESS = "GET_ALL_ADDRESS"

// ==================================== Get All Address ======================================//
export const GetAllAddress = (params) => {
    return async (dispatch) => {
        return await addressRouter.GetAllAddress(params)
            .then((response) => {
                dispatch({
                    type: GET_ALL_ADDRESS,
                    payload: {
                        data: response.data,
                        errorMessage: false,
                    },
                });
            })
            .catch((error) => {
                if (error.response && error.response.data) {
                    dispatch({
                        type: GET_ALL_ADDRESS,
                        payload: {
                            data: false,
                            errorMessage: error.response.data.message,
                        },
                    });
                } else {
                    dispatch({
                        type: GET_ALL_ADDRESS,
                        payload: {
                            data: false,
                            errorMessage: error.message,
                        },
                    });
                }
            });
    };
}
// ==================================== End Get All Address ======================================//