import addressRouter from "src/api/urlRoute/Address";

export const UPDATE_ADDRESS = "UPDATE_ADDRESS"

// ==================================== Update Address ======================================//
export const UpdateAddress = (id, data) => {
    return async (dispatch) => {
        return await addressRouter.UpdateAddress(id, data)
            .then((response) => {
                dispatch({
                    type: UPDATE_ADDRESS,
                    payload: {
                        data: response.data,
                        errorMessage: false,
                    },
                });
            })
            .catch((error) => {
                if (error.response && error.response.data) {
                    dispatch({
                        type: UPDATE_ADDRESS,
                        payload: {
                            data: false,
                            errorMessage: error.response.data.message,
                        },
                    });
                } else {
                    dispatch({
                        type: UPDATE_ADDRESS,
                        payload: {
                            data: false,
                            errorMessage: error.message,
                        },
                    });
                }
            });
    };
}
// ==================================== End Update Address ======================================//