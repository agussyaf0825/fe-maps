import addressRouter from "src/api/urlRoute/Address";

export const CREATE_ADDRESS = "CREATE_ADDRESS"

// ==================================== Create Address ======================================//
export const CreateAddress = (data) => {
    return async (dispatch) => {
        return await addressRouter.CreateAddress(data)
            .then((response) => {
                dispatch({
                    type: CREATE_ADDRESS,
                    payload: {
                        data: response.data,
                        errorMessage: false,
                    },
                });
            })
            .catch((error) => {
                if (error.response && error.response.data) {
                    dispatch({
                        type: CREATE_ADDRESS,
                        payload: {
                            data: false,
                            errorMessage: error.response.data.message,
                        },
                    });
                } else {
                    dispatch({
                        type: CREATE_ADDRESS,
                        payload: {
                            data: false,
                            errorMessage: error.message,
                        },
                    });
                }
            });
    };
}
// ==================================== End Create Address ======================================//