import addressRouter from "src/api/urlRoute/Address";

export const DELETE_ADDRESS = "DELETE_ADDRESS"

// ==================================== Delete Address ======================================//
export const DeleteAddress = (id, data) => {
    return async (dispatch) => {
        return await addressRouter.DeleteAddress(id, data)
            .then((response) => {
                dispatch({
                    type: DELETE_ADDRESS,
                    payload: {
                        data: response.data,
                        errorMessage: false,
                    },
                });
            })
            .catch((error) => {
                if (error.response && error.response.data) {
                    dispatch({
                        type: DELETE_ADDRESS,
                        payload: {
                            data: false,
                            errorMessage: error.response.data.message,
                        },
                    });
                } else {
                    dispatch({
                        type: DELETE_ADDRESS,
                        payload: {
                            data: false,
                            errorMessage: error.message,
                        },
                    });
                }
            });
    };
}
// ==================================== End Delete Address ======================================//