import addressRouter from "src/api/urlRoute/Address";

export const GET_ID_ADDRESS = "GET_ID_ADDRESS"

// ==================================== Get Id Address ======================================//
export const GetIdAddress = (id) => {
    return async (dispatch) => {
        return await addressRouter.GetIdAddress(id)
            .then((response) => {
                dispatch({
                    type: GET_ID_ADDRESS,
                    payload: {
                        data: response.data,
                        errorMessage: false,
                    },
                });
            })
            .catch((error) => {
                if (error.response && error.response.data) {
                    dispatch({
                        type: GET_ID_ADDRESS,
                        payload: {
                            data: false,
                            errorMessage: error.response.data.message,
                        },
                    });
                } else {
                    dispatch({
                        type: GET_ID_ADDRESS,
                        payload: {
                            data: false,
                            errorMessage: error.message,
                        },
                    });
                }
            });
    };
}
// ==================================== End Get Id Address ======================================//