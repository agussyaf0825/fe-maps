import statusRouter from "src/api/urlRoute/Status";

export const GET_ALL_STATUS = "GET_ALL_STATUS"

// ==================================== Get All Status ======================================//
export const GetAllStatus = (params) => {
    return async (dispatch) => {
        return await statusRouter.GetAllStatus(params)
            .then((response) => {
                dispatch({
                    type: GET_ALL_STATUS,
                    payload: {
                        data: response.data,
                        errorMessage: false,
                    },
                });
            })
            .catch((error) => {
                if (error.response && error.response.data) {
                    dispatch({
                        type: GET_ALL_STATUS,
                        payload: {
                            data: false,
                            errorMessage: error.response.data.message,
                        },
                    });
                } else {
                    dispatch({
                        type: GET_ALL_STATUS,
                        payload: {
                            data: false,
                            errorMessage: error.message,
                        },
                    });
                }
            });
    };
}
// ==================================== End Get All Status ======================================//