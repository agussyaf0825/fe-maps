import typeRouter from "src/api/urlRoute/Type";

export const GET_ALL_TYPE = "GET_ALL_TYPE"

// ==================================== Get All Type ======================================//
export const GetAllType = (params) => {
    return async (dispatch) => {
        return await typeRouter.GetAllType(params)
            .then((response) => {
                dispatch({
                    type: GET_ALL_TYPE,
                    payload: {
                        data: response.data,
                        errorMessage: false,
                    },
                });
            })
            .catch((error) => {
                if (error.response && error.response.data) {
                    dispatch({
                        type: GET_ALL_TYPE,
                        payload: {
                            data: false,
                            errorMessage: error.response.data.message,
                        },
                    });
                } else {
                    dispatch({
                        type: GET_ALL_TYPE,
                        payload: {
                            data: false,
                            errorMessage: error.message,
                        },
                    });
                }
            });
    };
}
// ==================================== End Get All Type ======================================//