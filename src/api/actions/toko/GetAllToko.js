import tokoRouter from "src/api/urlRoute/Toko";

export const GET_ALL_TOKO = "GET_ALL_TOKO"

// ==================================== Get All Toko ======================================//
export const GetAllToko = (params) => {
    return async (dispatch) => {
        return await tokoRouter.GetAllToko(params)
            .then((response) => {
                dispatch({
                    type: GET_ALL_TOKO,
                    payload: {
                        data: response.data,
                        errorMessage: false,
                    },
                });
            })
            .catch((error) => {
                if (error.response && error.response.data) {
                    dispatch({
                        type: GET_ALL_TOKO,
                        payload: {
                            data: false,
                            errorMessage: error.response.data.message,
                        },
                    });
                } else {
                    dispatch({
                        type: GET_ALL_TOKO,
                        payload: {
                            data: false,
                            errorMessage: error.message,
                        },
                    });
                }
            });
    };
}
// ==================================== End Get All Toko ======================================//