import tokoRouter from "src/api/urlRoute/Toko";

export const DELETE_TOKO = "DELETE_TOKO"

// ==================================== Delete Toko ======================================//
export const DeleteToko = (id, data) => {
    return async (dispatch) => {
        return await tokoRouter.DeleteToko(id, data)
            .then((response) => {
                dispatch({
                    type: DELETE_TOKO,
                    payload: {
                        data: response.data,
                        errorMessage: false,
                    },
                });
            })
            .catch((error) => {
                if (error.response && error.response.data) {
                    dispatch({
                        type: DELETE_TOKO,
                        payload: {
                            data: false,
                            errorMessage: error.response.data.message,
                        },
                    });
                } else {
                    dispatch({
                        type: DELETE_TOKO,
                        payload: {
                            data: false,
                            errorMessage: error.message,
                        },
                    });
                }
            });
    };
}
// ==================================== End Delete Toko ======================================//