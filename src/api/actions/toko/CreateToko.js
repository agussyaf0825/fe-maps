import tokoRouter from "src/api/urlRoute/Toko";

export const CREATE_TOKO = "CREATE_TOKO"

// ==================================== Create Toko ======================================//
export const CreateToko = (data) => {
    return async (dispatch) => {
        return await tokoRouter.CreateToko(data)
            .then((response) => {
                dispatch({
                    type: CREATE_TOKO,
                    payload: {
                        data: response.data,
                        errorMessage: false,
                    },
                });
            })
            .catch((error) => {
                if (error.response && error.response.data) {
                    dispatch({
                        type: CREATE_TOKO,
                        payload: {
                            data: false,
                            errorMessage: error.response.data.message,
                        },
                    });
                } else {
                    dispatch({
                        type: CREATE_TOKO,
                        payload: {
                            data: false,
                            errorMessage: error.message,
                        },
                    });
                }
            });
    };
}
// ==================================== End Create Toko ======================================//