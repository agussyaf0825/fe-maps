import tokoRouter from "src/api/urlRoute/Toko";

export const UPDATE_TOKO = "UPDATE_TOKO"

// ==================================== Update Toko ======================================//
export const UpdateToko = (id, data) => {
    return async (dispatch) => {
        return await tokoRouter.UpdateToko(id, data)
            .then((response) => {
                dispatch({
                    type: UPDATE_TOKO,
                    payload: {
                        data: response.data,
                        errorMessage: false,
                    },
                });
            })
            .catch((error) => {
                if (error.response && error.response.data) {
                    dispatch({
                        type: UPDATE_TOKO,
                        payload: {
                            data: false,
                            errorMessage: error.response.data.message,
                        },
                    });
                } else {
                    dispatch({
                        type: UPDATE_TOKO,
                        payload: {
                            data: false,
                            errorMessage: error.message,
                        },
                    });
                }
            });
    };
}
// ==================================== End Update Toko ======================================//