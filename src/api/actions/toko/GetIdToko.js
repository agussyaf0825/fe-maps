import tokoRouter from "src/api/urlRoute/Toko";

export const GET_ID_TOKO = "GET_ID_TOKO"

// ==================================== Get Id Toko ======================================//
export const GetIdToko = (id) => {
    return async (dispatch) => {
        return await tokoRouter.GetIdToko(id)
            .then((response) => {
                dispatch({
                    type: GET_ID_TOKO,
                    payload: {
                        data: response.data,
                        errorMessage: false,
                    },
                });
            })
            .catch((error) => {
                if (error.response && error.response.data) {
                    dispatch({
                        type: GET_ID_TOKO,
                        payload: {
                            data: false,
                            errorMessage: error.response.data.message,
                        },
                    });
                } else {
                    dispatch({
                        type: GET_ID_TOKO,
                        payload: {
                            data: false,
                            errorMessage: error.message,
                        },
                    });
                }
            });
    };
}
// ==================================== End Get Id Toko ======================================//