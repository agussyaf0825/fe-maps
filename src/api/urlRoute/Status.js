import axios from "src/api/config/index"

const Status = {
    // ================ status ================ //
    GetAllStatus: (urlParams) => axios.get('/status', { params: urlParams }),
}
export default Status;