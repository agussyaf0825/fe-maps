import axios from "src/api/config/index"

const Type = {
    // ================ Type ================ //
    GetAllType: (urlParams) => axios.get('/type', { params: urlParams }),
}
export default Type;