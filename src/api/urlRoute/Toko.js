import axios from "src/api/config/index"

const Toko = {
    // ================ Toko ================ //
    GetAllToko: (urlParams) => axios.get('/toko', { params: urlParams }),
    GetIdToko: (id) => axios.get('/toko/' + id),
    CreateToko: (data) => axios.post('/toko', data),
    UpdateToko: (id, data) => axios.put('/toko/' + id, data),
    DeleteToko: (id, data) => axios.delete('/toko/' + id, { data }),
}
export default Toko;