import axios from "src/api/config/index"

const Daerah = {

    // ================ Daerah ================ //
    // ================ Provinsi ================ //
    GetAllProvinsi: (urlParams) => axios.get('/daerah/indonesia/provinsi', { params: urlParams }),
    GetIdProvinsi: (id) => axios.get('/daerah/indonesia/provinsi/' + id),
    // ================ Kota Kabupaten ================ //
    GetAllKotaKabupaten: (urlParams) => axios.get('/daerah/indonesia/kota-kabupaten', { params: urlParams }),
    GetIdKotaKabupaten: (id) => axios.get('/daerah/indonesia/kota-kabupaten/' + id),
    // ================ Kecamatan ================ //
    GetAllKecamatan: (urlParams) => axios.get('/daerah/indonesia/kecamatan', { params: urlParams }),
    GetIdKecamatan: (id) => axios.get('/daerah/indonesia/kecamatan/' + id),
    // ================ Kelurahan ================ //
    GetAllKelurahan: (urlParams) => axios.get('/daerah/indonesia/kelurahan', { params: urlParams }),
    GetIdKelurahan: (id) => axios.get('/daerah/indonesia/kelurahan/' + id),

}
export default Daerah;