import axios from "src/api/config/index"

const Address = {
    // ================ Address ================ //
    GetAllAddress: (urlParams) => axios.get('/address', { params: urlParams }),
    GetIdAddress: (id) => axios.get('/address/' + id),
    CreateAddress: (data) => axios.post('/address', data),
    UpdateAddress: (id, data) => axios.put('/address/' + id, data),
    DeleteAddress: (id, data) => axios.delete('/address/' + id, { data }),
}
export default Address;