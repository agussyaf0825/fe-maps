import { toast } from "react-toastify";

export default function errorHandler(error) {
  if (error) {
    if (error.message === "Network Error") {
      toast.error("Connection Not Available, Please Contact Your Support")
    }
    let message;
    if (error.response) {
      if (error.response.status === 500) {
        message = "Something went terribly wrong";
      }
      if (error.response.status === 401) {
        localStorage.clear()
        sessionStorage.clear()
        window.location.reload()
        this.props.history.replace("/login")
      }
      else message = error.response.data.message;
      if (typeof message === "string") toast.error(message);
    }
    return Promise.reject(error);
  }
}
