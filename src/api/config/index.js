
import axios from "axios";
import errorHandler from "./errorHandler";


const instance = axios.create({
  baseURL: `${process.env.REACT_APP_API_GATEWAY}`,
  headers: {
    'Access-Control-Allow-Origin': '*',
    "Content-Type": 'application/json',
  },
});

instance.interceptors.response.use((response) => response.data, errorHandler);
export default instance;