module.exports = {
    name: "Dashboard-Robocall",
    script: "serve",
    env: {
      PM2_SERVE_PATH: 'build',
      PM2_SERVE_PORT: 3030,
      PM2_SERVE_SPA: 'true',
    }
  }